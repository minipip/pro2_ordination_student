package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Test;

public class DagligSkaevTest {

    DagligSkaev ds;
    Patient p;

    @Test
    public void testSamletDosis() {
        ArrayList<Dosis> doser = new ArrayList<>();
        
        LocalDate start = LocalDate.of(2017, 2, 28);
        LocalDate slut = LocalDate.of(2017, 3, 2);
        p = new Patient("120288-1242", "Tester", 80);
        
        ds = new DagligSkaev(start, slut, p);
        
        Dosis d1 = ds.createDosis(LocalTime.of(10, 30), 2);
        Dosis d2 = ds.createDosis(LocalTime.of(12, 30), 2);
        Dosis d3 = ds.createDosis(LocalTime.of(14, 30), 1);
        doser.add(d1);
        doser.add(d2);
        doser.add(d3);
        
        double res = ds.samletDosis();
        
        assertEquals(15, res, 0.00001);
    }

    @Test
    public void testDoegnDosis() {
        ArrayList<Dosis> doser = new ArrayList<>();
        
        LocalDate start = LocalDate.of(2017, 2, 28);
        LocalDate slut = LocalDate.of(2017, 3, 2);
        p = new Patient("120288-1242", "Tester", 80);
        
        ds = new DagligSkaev(start, slut, p);
        
        Dosis d1 = ds.createDosis(LocalTime.of(10, 30), 2);
        Dosis d2 = ds.createDosis(LocalTime.of(12, 30), 2);
        Dosis d3 = ds.createDosis(LocalTime.of(14, 30), 1);
        doser.add(d1);
        doser.add(d2);
        doser.add(d3);
        
        double res = ds.doegnDosis();
        
        assertEquals(5, res, 0.00001);
    }

    @Test
    public void testGetType() {
        LocalDate start = LocalDate.of(2017, 2, 28);
        LocalDate slut = LocalDate.of(2017, 3, 3);
        
        p = new Patient("121256-0512", "Jane Jensen", 63.6);
        
        ds = new DagligSkaev(start, slut, p);
        
        String res = ds.getType();
        
        assertEquals("Daglig skæv", res);

    }

    /*
     *  * Pre: startDen, slutDen, patient og laegemiddel er ikke null
     *  startDen er en dato før slutDen
     */
    @Test
    public void testDagligSkaev() {
        LocalDate start = LocalDate.of(2017, 2, 28);
        LocalDate slut = LocalDate.of(2017, 3, 14);

        p = new Patient("121256-0512", "Jane Jensen", 63.6);

        ds = new DagligSkaev(start, slut, p);
        
        Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        ds.setLaegemiddel(l);

        DagligSkaev res = ds;

        assertEquals(start, ds.getStartDen());
        assertEquals(slut, ds.getSlutDen());
        assertEquals(l, ds.getLaegemiddel());
        assertNotNull(res);
    }

    @Test
    public void testGetDoser() {
        LocalDate start = LocalDate.of(2017, 2, 28);
        LocalDate slut = LocalDate.of(2017, 3, 3);

        p = new Patient("121256-0512", "Jane Jensen", 63.6);

        ds = new DagligSkaev(start, slut, p);

        Dosis d1 = ds.createDosis(LocalTime.of(10, 30), 1);
        Dosis d2 = ds.createDosis(LocalTime.of(12, 30), 3);
        Dosis d3 = ds.createDosis(LocalTime.of(14, 30), 3);
        Dosis d4 = ds.createDosis(LocalTime.of(16, 30), 4);
        ArrayList<Dosis> doser = new ArrayList<>();

        doser.add(d1);
        doser.add(d2);
        doser.add(d3);
        doser.add(d4);
        
        assertEquals(doser, ds.getDoser());
    }

    @Test
    public void testSetDoser() {
        LocalDate start = LocalDate.of(2017, 2, 28);
        LocalDate slut = LocalDate.of(2017, 3, 3);

        p = new Patient("121256-0512", "Jane Jensen", 63.6);

        ds = new DagligSkaev(start, slut, p);

        ArrayList<Dosis> expectedList = new ArrayList<>();
        expectedList.add(new Dosis(LocalTime.of(10, 30), 1));
        
        ds.setDoser(expectedList);

        assertEquals(expectedList, ds.getDoser());
    }

    @Test
    public void testCreateDosis() {
        p = new Patient("120288-1242", "Tester", 80);
        
        LocalDate start1 = LocalDate.of(2017, 2, 28);
        LocalDate slut2 = LocalDate.of(2017, 2, 28);

        ds = new DagligSkaev(start1, slut2, p);
        
        ds.createDosis(LocalTime.of(12, 30), 3);
        
        assertEquals(1, ds.getDoser().size());
    }

    @Test
    public void testDeleteDosis() {
        p = new Patient("120288-1242", "Tester", 80);
        
        LocalDate start1 = LocalDate.of(2017, 2, 28);
        LocalDate slut2 = LocalDate.of(2017, 2, 28);

        ds = new DagligSkaev(start1, slut2, p);
        
        Dosis d1 = ds.createDosis(LocalTime.of(10, 30), 1);

        ds.deleteDosis(d1);
        
        assertEquals(0, ds.getDoser().size());
    }
    
    @Test
    public void testGetStartDen() {
        LocalDate start = LocalDate.of(2017, 2, 27);
        LocalDate slut = LocalDate.of(2017, 3, 28);

        p = new Patient("121256-0512", "Jane Jensen", 63.6);

        ds = new DagligSkaev(start, slut, p);
        
        assertEquals(start, ds.getStartDen());
    }

    @Test
    public void testGetSlutDen() {
        LocalDate start = LocalDate.of(2017, 2, 27);
        LocalDate slut = LocalDate.of(2017, 3, 28);

        p = new Patient("121256-0512", "Jane Jensen", 63.6);

        ds = new DagligSkaev(start, slut, p);
        
        assertEquals(slut, ds.getSlutDen());
    }

    @Test
    public void testSetLaegemiddel() {
        LocalDate start = LocalDate.of(2017, 2, 28);
        LocalDate slut = LocalDate.of(2017, 3, 3);
        
        Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        
        p = new Patient("121256-0512", "Jane Jensen", 63.6);

        ds = new DagligSkaev(start, slut, p);
        ds.setLaegemiddel(l);
        
        assertEquals(l, ds.getLaegemiddel());
    }

    @Test
    public void testGetLaegemiddel() {
        LocalDate start = LocalDate.of(2017, 2, 28);
        LocalDate slut = LocalDate.of(2017, 3, 3);
        
        Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        
        p = new Patient("121256-0512", "Jane Jensen", 63.6);

        ds = new DagligSkaev(start, slut, p);
        ds.setLaegemiddel(l);
        
        assertEquals(l, ds.getLaegemiddel());
    }

    @Test
    public void testAntalDage() {
        LocalDate start = LocalDate.of(2017, 2, 28);
        LocalDate slut = LocalDate.of(2017, 3, 1);
        
        p = new Patient("120288-1242", "Tester", 80);
        
        ds = new DagligSkaev(start, slut, p);
        
        int actualResult = ds.antalDage();

        assertEquals(2, actualResult);
    }

    @Test
    public void testAntalDage2() {
        
        p = new Patient("120288-1242", "Tester", 80);
        
        LocalDate start1 = LocalDate.of(2017, 2, 28);
        LocalDate slut2 = LocalDate.of(2017, 2, 28);

        ds = new DagligSkaev(start1, slut2, p);

        int actualResult = ds.antalDage();

        assertEquals(1, actualResult);

    }

    @Test
    public void testToString() {
        p = new Patient("120288-1242", "Tester", 80);
        
        LocalDate start1 = LocalDate.of(2017, 2, 28);
        LocalDate slut2 = LocalDate.of(2017, 2, 28);

        ds = new DagligSkaev(start1, slut2, p);

        assertEquals(start1.toString(), ds.toString());
    }

}
