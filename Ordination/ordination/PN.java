package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class PN extends Ordination {

    private double antalEnheder;
    private List<LocalDate> antalGivet;

    public PN(LocalDate start, LocalDate slut, Patient patient, double antalEnheder) {
        super(start, slut, patient);
        this.antalEnheder = antalEnheder;
        antalGivet = new ArrayList<>();
    }

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen
     * Returnerer true hvis givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
     * Retrurner false ellers og datoen givesDen ignoreres
     * @param givesDen
     * @return
     */
    public boolean givDosis(LocalDate givesDen) {
        boolean betweenDate = false;

        if ((givesDen.isAfter(getStartDen()) || givesDen.equals(getStartDen()))
            && (givesDen.isBefore(getSlutDen()) || givesDen.equals(getSlutDen()))) {
            antalGivet.add(givesDen);
            betweenDate = true;
        }

        return betweenDate;
    }
    
    @Override
    public double doegnDosis() {

        double diff = 0;

        if (antalGivet.size() > 0) {
            
            LocalDate first = antalGivet.get(0);
            LocalDate last = antalGivet.get(1);
            
            for (int i = 0; i < antalGivet.size(); i++) {
                if (antalGivet.get(i).isBefore(first)) {
                    first = antalGivet.get(i);
                }
                else if (antalGivet.get(i).isAfter(last)) {
                    last = antalGivet.get(i);
                }
            }

            diff = (getAntalGangeGivet() * antalEnheder) / ChronoUnit.DAYS.between(first, last);
        }

        return diff;
    }

    @Override
    public double samletDosis() {
        return doegnDosis() * antalDage();
    }

    /**
     * Returnerer antal gange ordinationen er anvendt
     * @return
     */
    public int getAntalGangeGivet() {
        return antalGivet.size();
    }
    
    public double getAntalEnheder() {
        return antalEnheder;
    }

    @Override
    public String getType() {
        return "PN";
    }
}
