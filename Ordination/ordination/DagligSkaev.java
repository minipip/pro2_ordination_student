package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
    private ArrayList<Dosis> dosiser;

    public DagligSkaev(LocalDate start, LocalDate slut, Patient patient) {
        super(start, slut, patient);
        dosiser = new ArrayList<>();
    }
    
    @Override
    public double samletDosis() {
        return doegnDosis() * antalDage();
    }
    
    @Override
    public double doegnDosis() {
        double sum = 0;
        for (Dosis d : dosiser) {
            sum += d.getAntal();
        }
        return sum;
    }
    
    @Override
    public String getType() {
        return "Daglig skæv";
    }

    //----------------------------------------------------------------------------
    
    public ArrayList<Dosis> getDoser() {
        return new ArrayList<>(dosiser);
    }

    public void setDoser(ArrayList<Dosis> dosiser) {
        this.dosiser = dosiser;
    }

    public Dosis createDosis(LocalTime tid, double antal) {
        Dosis dosis = new Dosis(tid, antal);
        dosiser.add(dosis);
        return dosis;
    }

    public void deleteDosis(Dosis dosis) {
        dosiser.remove(dosis);
    }

}
