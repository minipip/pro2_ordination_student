package ordination;

import java.time.*;

public class DagligFast extends Ordination {

    private Dosis[] doser;

    public DagligFast(LocalDate start, LocalDate slut, Patient patient, double morgen,
        double middag, double aften, double nat) {
        super(start, slut, patient);
        doser = new Dosis[4];
        doser[0] = createDosis(LocalTime.of(6, 00), morgen);
        doser[1] = createDosis(LocalTime.of(12, 00), middag);
        doser[2] = createDosis(LocalTime.of(18, 00), aften);
        doser[3] = createDosis(LocalTime.of(00, 00), nat);
    }

    @Override
    public double samletDosis() {
        double result;

        result = super.antalDage();
        result *= doegnDosis();
        return result;

    }

    public Dosis createDosis(LocalTime tid, double antal) {
        Dosis d = new Dosis(tid, antal);
        return d;
    }

    public Dosis[] getDoser() {
        return doser;
    }
    
    public void setDoser(Dosis[] dosiser) {
        this.doser = dosiser;
    }
    
    @Override
    public double doegnDosis() {
        double result = 0;
        for (Dosis d : doser) {
            result += d.getAntal();
        }
        return result;
    }

    @Override
    public String getType() {
        return "Daglig Fast";
    }
    
}
