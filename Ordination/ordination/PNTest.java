package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;

public class PNTest {
    Patient p1;
    Laegemiddel lm;
    
    @Before
    public void setUp() throws Exception {
        p1 = new Patient("121256-0512", "Jane Jensen", 63.4);
        lm = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
    }
    
    @Test
    public void testConstructer() {
        LocalDate start = LocalDate.ofYearDay(2017, 27);
        LocalDate slut = LocalDate.ofYearDay(2017, 29);

        PN p = new PN(start, slut, p1, 2);
        p.setLaegemiddel(lm);

        assertEquals(LocalDate.ofYearDay(2017, 27), p.getStartDen());
        assertEquals(2.0, p.getAntalEnheder(), 0.1);
        assertEquals(lm, p.getLaegemiddel());
        assertNotNull(p);
    }
    
    @Test
    public void testGivDosis() {
        LocalDate start = LocalDate.ofYearDay(2017, 27);
        LocalDate slut = LocalDate.ofYearDay(2017, 29);

        PN p = new PN(start, slut, p1, 2);
        p.setLaegemiddel(lm);

        assertTrue(p.givDosis(LocalDate.ofYearDay(2017, 28)));
        
    }

    @Test
    public void testDoegnDosis() {
        LocalDate start = LocalDate.ofYearDay(2017, 27);
        LocalDate slut = LocalDate.ofYearDay(2017, 31);

        PN p = new PN(start, slut, p1, 2);
        p.setLaegemiddel(lm);

        p.givDosis(LocalDate.ofYearDay(2017, 31));
        
        p.givDosis(LocalDate.ofYearDay(2017, 28));
        p.givDosis(LocalDate.ofYearDay(2017, 28));
        p.givDosis(LocalDate.ofYearDay(2017, 28));
        p.givDosis(LocalDate.ofYearDay(2017, 28));
        p.givDosis(LocalDate.ofYearDay(2017, 28));
        p.givDosis(LocalDate.ofYearDay(2017, 28));
        p.givDosis(LocalDate.ofYearDay(2017, 28));
        p.givDosis(LocalDate.ofYearDay(2017, 28));

        p.givDosis(LocalDate.ofYearDay(2017, 27));
        p.givDosis(LocalDate.ofYearDay(2017, 27));
        p.givDosis(LocalDate.ofYearDay(2017, 27));

        assertEquals(6.0, p.doegnDosis(), 0.1);
        
    }
    
    @Test
    public void testSamletDosis() {
        LocalDate start = LocalDate.ofYearDay(2017, 27);
        LocalDate slut = LocalDate.ofYearDay(2017, 34);
        
        PN p = new PN(start, slut, p1, 2);
        p.setLaegemiddel(lm);
        
        p.givDosis(LocalDate.ofYearDay(2017, 31));

        p.givDosis(LocalDate.ofYearDay(2017, 28));
        p.givDosis(LocalDate.ofYearDay(2017, 28));
        p.givDosis(LocalDate.ofYearDay(2017, 28));
        p.givDosis(LocalDate.ofYearDay(2017, 28));
        p.givDosis(LocalDate.ofYearDay(2017, 28));
        p.givDosis(LocalDate.ofYearDay(2017, 28));
        p.givDosis(LocalDate.ofYearDay(2017, 28));
        p.givDosis(LocalDate.ofYearDay(2017, 28));
        
        p.givDosis(LocalDate.ofYearDay(2017, 27));
        p.givDosis(LocalDate.ofYearDay(2017, 27));
        p.givDosis(LocalDate.ofYearDay(2017, 27));
        
        assertEquals(48.0, p.samletDosis(), 0.1);

    }
    
}
