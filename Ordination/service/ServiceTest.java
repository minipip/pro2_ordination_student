package service;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;

public class ServiceTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testOpretPNOrdination() {
        Patient p1 = new Patient("jane jensen", "121256-0512", 63.6);
        LocalDate Start = LocalDate.ofYearDay(2017, 58);
        LocalDate Slut = LocalDate.ofYearDay(2017, 87);

        Laegemiddel lm = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        PN p = null;
        p = Service.getTestService().opretPNOrdination(Start, Slut, p1, lm, 13);

        assertEquals(LocalDate.ofYearDay(2017, 58), p.getStartDen());
        assertEquals(lm, p.getLaegemiddel());
        assertEquals(13, p.getAntalEnheder(), 0.1);
        assertNotNull(p);
    }
    
    @Test
    public void testOpretDagligFastOrdination() {
        Patient p1 = new Patient("jane jensen", "121256-0512", 63.6);
        LocalDate Start = LocalDate.ofYearDay(2017, 58);
        LocalDate Slut = LocalDate.ofYearDay(2017, 87);
        Laegemiddel lm = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        
        DagligFast df = null;
        df = Service.getTestService().opretDagligFastOrdination(Start, Slut, p1, lm, 1, 3, 0, 8);
        
        assertEquals(360, df.samletDosis(), 1);
        assertEquals(lm, df.getLaegemiddel());
        assertNotNull(df);

    }
    
    @Test
    public void testOpretdagligskaevOrdination() {
        Patient p1 = new Patient("jane jensen", "121256-0512", 63.6);
        LocalDate Start = LocalDate.ofYearDay(2017, 58);
        LocalDate Slut = LocalDate.ofYearDay(2017, 87);
        Laegemiddel lm = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");

        LocalTime[] k = new LocalTime[3];
        k[0] = LocalTime.parse("10:30", DateTimeFormatter.ISO_LOCAL_TIME);
        k[1] = LocalTime.parse("14:30", DateTimeFormatter.ISO_LOCAL_TIME);
        k[2] = LocalTime.parse("18:30", DateTimeFormatter.ISO_LOCAL_TIME);

        double[] enheder = new double[3];
        enheder[0] = 3.0;
        enheder[1] = 3.0;
        enheder[2] = 1.0;
        
        DagligSkaev ds = null;
        ds = Service.getTestService().opretDagligSkaevOrdination(Start, Slut, p1, lm, k, enheder);
        
        assertEquals(7, ds.doegnDosis(), 1);
        assertEquals(lm, ds.getLaegemiddel());
        assertEquals(210, ds.samletDosis(), 1);
        assertNotNull(ds);

    }
    
    @Test
    public void testOrdinationPNAnvendt() {
        Patient p1 = new Patient("jane jensen", "121256-0512", 63.6);
        LocalDate Start = LocalDate.ofYearDay(2017, 59);
        LocalDate Slut = LocalDate.ofYearDay(2017, 87);
        Laegemiddel lm = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        
        LocalDate dato = LocalDate.ofYearDay(2017, 62);
        PN p = new PN(Start, Slut, p1, 4);
        
        Service.getTestService().ordinationPNAnvendt(p, dato);
        
        assertNotNull(p.getAntalGangeGivet());

    }
    
    @Test
    public void testAntalOrdinationerPrVægtPrLægemiddel() {

        Patient p1 = Service.getService().opretPatient("121256-0512", "jane jensen", 63.6);
        Patient p2 =
            Service.getService().opretPatient("280295-9322", "Susanne Henriksen", 125.3);
        Patient p3 = Service.getService().opretPatient("130996-3049", "Marcus Thomsen", 41.9);
        Patient p4 = Service.getService().opretPatient("190399-1021", "Casper Juul", 40.2);

        LocalDate Start = LocalDate.ofYearDay(2017, 59);
        LocalDate Slut = LocalDate.ofYearDay(2017, 87);
        Laegemiddel lm = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");

        Service.getService().opretPNOrdination(Start, Slut, p1, lm, 1);
        Service.getService().opretPNOrdination(Start, Slut, p2, lm, 1);
        Service.getService().opretPNOrdination(Start, Slut, p3, lm, 1);
        Service.getService().opretPNOrdination(Start, Slut, p4, lm, 1);

        int g = Service.getService().antalOrdinationerPrVægtPrLægemiddel(40, 70, lm);

        assertEquals(3, g);

    }

}
